import { Component } from 'react';
import CameraWizard from 'vc-camera-wizard';

class ConnectCamera extends Component {
  render() {
    return <CameraWizard />;
  }
}

export default ConnectCamera;
