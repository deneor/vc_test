import ReactOnRails from 'react-on-rails';

import ConnectCamera from './ConnectCamera';

ReactOnRails.register({
  ConnectCamera,
});
