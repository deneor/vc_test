// Common client-side webpack configuration used by webpack.hot.config and webpack.rails.config.

const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const environments = ['NODE_ENV'];
const root = process.cwd();
const WEBPACK_SPRITES_PATH = 'assets/svg';
const HtmlWebpackPlugin = require('html-webpack-plugin');

environments.forEach((name) => {
  console.log(`${name}: ${process.env[name]}`);
});

module.exports = {
  // the project dir
  context: __dirname,
  entry: {
    // See use of 'vendor' in the CommonsChunkPlugin inclusion below.
    vendor: ['jquery'],

    // This will contain the app entry points defined by webpack.hot.config and
    // webpack.rails.config
    app: ['./app/clientGlobals'],
  },
  resolve: {
    modules: [root, path.resolve(root, 'src'), 'node_modules'],
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.EnvironmentPlugin(environments),

    // https://webpack.github.io/docs/list-of-plugins.html#2-explicit-vendor-chunk
    new webpack.optimize.CommonsChunkPlugin({
      // This name 'vendor' ties into the entry definition
      name: 'vendor',

      // We don't want the default vendor.js name
      filename: 'vendor-bundle.js',

      // Passing Infinity just creates the commons chunk, but moves no modules into it.
      // In other words, we only put what's in the vendor entry definition in vendor-bundle.js
      minChunks: Infinity,
    }),
    new webpack.ContextReplacementPlugin(/node_modules\/moment\/locale/, /ru/),
    new HtmlWebpackPlugin(),
  ],
  resolveLoader: {
    moduleExtensions: ['-loader'],
  },
  module: {
    loaders: [
      { loader: 'babel-loader', exclude: /\/node_modules\// },
      // Not all apps require jQuery. Many Rails apps do, such as those using TurboLinks or
      // bootstrap js
      { test: require.resolve('jquery'), loader: 'expose-loader?jQuery' },
      { test: require.resolve('jquery'), loader: 'expose-loader?$' },
    ],
  },
};
