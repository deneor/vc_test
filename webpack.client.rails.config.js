// NOTE: All style sheets handled by the asset pipeline in rails

const config = require('./webpack.client.base.config');

const devBuild = process.env.NODE_ENV !== 'production';

var path = require('path'),
  ASSET_PATH = path.join(__dirname, './public');

config.output = {
  filename: '[name]-bundle.js',
  path: ASSET_PATH,
};

// You can add entry points specific to rails here
// The es5-shim/sham is for capybara testing
// config.entry.vendor.unshift(
//   'es5-shim/es5-shim',
//   'es5-shim/es5-sham'
// );

// See webpack.common.config for adding modules common to both the webpack dev server and rails
config.module.loaders.push(
  {
    test: /\.jsx?$/,
    loader: 'babel-loader',
    exclude: /node_modules/,
  },
  {
    test: require.resolve('react'),
    loader: 'expose?React',
  },
  {
    test: require.resolve('react-dom'),
    loader: 'expose?ReactDOM',
  }
);

module.exports = config;
